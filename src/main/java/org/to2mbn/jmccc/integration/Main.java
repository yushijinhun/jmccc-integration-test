package org.to2mbn.jmccc.integration;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.to2mbn.jmccc.mcdownloader.RemoteVersion;
import org.to2mbn.jmccc.mcdownloader.RemoteVersionList;
import org.to2mbn.jmccc.mcdownloader.provider.forge.ForgeDownloadProvider;
import org.to2mbn.jmccc.mcdownloader.provider.forge.ForgeVersion;
import org.to2mbn.jmccc.mcdownloader.provider.forge.ForgeVersionList;
import org.to2mbn.jmccc.mcdownloader.provider.liteloader.LiteloaderDownloadProvider;
import org.to2mbn.jmccc.mcdownloader.provider.liteloader.LiteloaderVersion;
import org.to2mbn.jmccc.mcdownloader.provider.liteloader.LiteloaderVersionList;

public class Main {

	private static Set<String> getBlacklistedVersions() {
		Set<String> versions = new LinkedHashSet<>();
		versions.add("1.6.2-forge1.6.2-9.10.1.871-LiteLoader1.6.2");
		versions.add("1.7.10-forge1.7.10-10.13.4.1558-LiteLoader1.7.10");
		versions.add("1.7.2-forge1.7.2-10.12.2.1121-LiteLoader1.7.2");
		versions.add("1.7.2-forge1.7.2-10.12.2.1121");
		versions.add("1.7.2-forge1.7.2-10.12.2.1147");
		versions.add("1.7.2-forge1.7.2-10.12.2.1147-LiteLoader1.7.2");
		return versions;
	}

	private static Set<String> getVersionsToTest(RemoteVersionList mcVersions, ForgeVersionList forgeVersions, LiteloaderVersionList liteloaderVersions) {
		Set<String> versions = new LinkedHashSet<>();

		Set<LiteloaderVersion> allLiteloaderVersions = new LinkedHashSet<>();
		allLiteloaderVersions.addAll(liteloaderVersions.getSnapshots().values());
		allLiteloaderVersions.addAll(liteloaderVersions.getLatests().values());
		allLiteloaderVersions.forEach(liteloader -> {
			ForgeVersion latestForge = forgeVersions.getLatest(liteloader.getMinecraftVersion());
			if (latestForge != null)
				versions.add(liteloader.customize(latestForge.getVersionName()).getVersionName());
		});

		allLiteloaderVersions.stream().map(LiteloaderVersion::getVersionName).forEach(versions::add);
		forgeVersions.getLatests().values().stream().map(ForgeVersion::getVersionName).forEach(versions::add);
		versions.add(mcVersions.getLatestSnapshot());
		mcVersions.getVersions().values().stream().filter(v -> "release".equals(v.getType())).map(RemoteVersion::getVersion).forEach(versions::add);

		return versions;
	}

	public static void main(String[] args) throws Exception {
		LaunchHandler service = new LaunchHandler();
		RemoteVersionList mcVersions = service.getDownloader().fetchRemoteVersionList(null).get();
		ForgeVersionList forgeVersions = service.getDownloader().download(new ForgeDownloadProvider().forgeVersionList(), null).get();
		LiteloaderVersionList liteloaderVersions = service.getDownloader().download(new LiteloaderDownloadProvider().liteloaderVersionList(), null).get();
		Set<String> versions = getVersionsToTest(mcVersions, forgeVersions, liteloaderVersions);
		versions.removeAll(getBlacklistedVersions());
		System.err.println(">> Versions to test (" + versions.size() + "): " + versions);
		System.err.println();
		Map<String, Object> results = new HashMap<>();
		for (String version : versions) {
			System.err.println("====================");
			System.err.println("Testing " + version);
			for (int retries = 0; retries < 3; retries++) {
				if (retries != 0) {
					System.err.println();
					System.err.println("Retrying...");
				}
				try {
					long t0 = System.currentTimeMillis();
					service.testJmccc(new File("minecraft-test"), version);
					long t1 = System.currentTimeMillis();
					results.put(version, Long.valueOf(t1 - t0));
					break;
				} catch (TestFailureException e) {
					results.put(version, e);
					System.err.println();
					System.err.println("Caught an exception:");
					e.printStackTrace();
				}
			}
			System.err.println("====================");
			System.err.println();
		}
		System.err.println("====================");
		System.err.println("Statistics");
		System.err.println();
		boolean succeed = true;
		for (String version : versions) {
			Object result = results.get(version);
			if (result instanceof Long) {
				System.err.println(version + "\t\tPASSED (" + result + "ms)");
			} else if (result instanceof Throwable) {
				succeed = false;
				System.err.println(version + "\t\tFAILURE (" + (((Throwable) result).getCause()) + ")");
			}
		}
		System.err.println("====================");
		service.shutdown();
		System.exit(succeed ? 0 : 1);
	}

}
