package org.to2mbn.jmccc.integration;

public class TestFailureException extends Exception {

	private static final long serialVersionUID = 1L;

	public TestFailureException() {}

	public TestFailureException(String message) {
		super(message);
	}

	public TestFailureException(Throwable cause) {
		super(cause);
	}

	public TestFailureException(String message, Throwable cause) {
		super(message, cause);
	}

}
