package org.to2mbn.jmccc.integration;

import java.io.File;
import java.net.URI;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.to2mbn.jmccc.auth.OfflineAuthenticator;
import org.to2mbn.jmccc.launch.ProcessListener;
import org.to2mbn.jmccc.launch.LaunchException;
import org.to2mbn.jmccc.launch.Launcher;
import org.to2mbn.jmccc.launch.LauncherBuilder;
import org.to2mbn.jmccc.mcdownloader.MinecraftDownloader;
import org.to2mbn.jmccc.mcdownloader.MinecraftDownloaderBuilder;
import org.to2mbn.jmccc.mcdownloader.download.Downloader;
import org.to2mbn.jmccc.mcdownloader.download.DownloaderBuilders;
import org.to2mbn.jmccc.mcdownloader.download.cache.CachedDownloaderBuilder;
import org.to2mbn.jmccc.mcdownloader.download.combine.CombinedDownloadTask;
import org.to2mbn.jmccc.mcdownloader.download.combine.CombinedDownloadTask.CacheStrategy;
import org.to2mbn.jmccc.mcdownloader.download.combine.CombinedDownloader;
import org.to2mbn.jmccc.mcdownloader.download.combine.CombinedDownloaderBuilder;
import org.to2mbn.jmccc.mcdownloader.download.concurrent.CombinedDownloadCallback;
import org.to2mbn.jmccc.mcdownloader.download.concurrent.DownloadCallback;
import org.to2mbn.jmccc.mcdownloader.download.concurrent.DownloadCallbacks;
import org.to2mbn.jmccc.mcdownloader.download.tasks.DownloadTask;
import org.to2mbn.jmccc.mcdownloader.provider.DownloadProviderChain;
import org.to2mbn.jmccc.mcdownloader.provider.MojangDownloadProvider;
import org.to2mbn.jmccc.mcdownloader.provider.forge.ForgeDownloadProvider;
import org.to2mbn.jmccc.mcdownloader.provider.liteloader.LiteloaderDownloadProvider;
import org.to2mbn.jmccc.option.LaunchOption;
import org.to2mbn.jmccc.option.MinecraftDirectory;
import org.to2mbn.jmccc.util.Builder;
import org.to2mbn.jmccc.util.ExtraArgumentsTemplates;
import org.to2mbn.jmccc.version.Version;

public class LaunchHandler {

	private static final long LAUNCH_WAIT_TIME = 20 * 1000;

	private class ProxiedDownloadCallback<T> implements DownloadCallback<T> {

		private DownloadCallback<T> proxied;
		private DownloadTask<T> task;

		public ProxiedDownloadCallback(DownloadCallback<T> proxied, DownloadTask<T> task) {
			this.proxied = proxied == null ? DownloadCallbacks.empty() : proxied;
			this.task = task;
		}

		@Override
		public void done(T result) {
			System.err.println("* Downloaded: " + task.getURI());
			proxied.done(result);
		}

		@Override
		public void updateProgress(long done, long total) {
			proxied.updateProgress(done, total);
		}

		@Override
		public void failed(Throwable e) {
			synchronized (System.err) {
				System.err.println("* Failed: " + task.getURI());
				e.printStackTrace();
			}
			proxied.failed(e);
		}

		@Override
		public void retry(Throwable e, int current, int max) {
			synchronized (System.err) {
				System.err.println("* Retry (" + current + "/" + max + "): " + task.getURI());
				e.printStackTrace();
			}
			proxied.retry(e, current, max);
		}

		@Override
		public void cancelled() {
			System.err.println("* Cancelled: " + task.getURI());
			proxied.cancelled();
		}

	}

	private class ProxiedDownloader implements Downloader {

		private Downloader proxied;

		public ProxiedDownloader(Downloader proxied) {
			this.proxied = proxied;
		}

		@Override
		public void shutdown() {
			proxied.shutdown();
		}

		@Override
		public boolean isShutdown() {
			return proxied.isShutdown();
		}

		@Override
		public <T> Future<T> download(DownloadTask<T> task, DownloadCallback<T> callback) {
			return proxied.download(task, new ProxiedDownloadCallback<>(callback, task));
		}

		@Override
		public <T> Future<T> download(DownloadTask<T> task, DownloadCallback<T> callback, int tries) {
			return proxied.download(task, new ProxiedDownloadCallback<>(callback, task), tries);
		}

	}

	private class ProxiedCombinedDownloader implements CombinedDownloader {

		private CombinedDownloader proxied;

		public ProxiedCombinedDownloader(CombinedDownloader proxied) {
			this.proxied = proxied;
		}

		@Override
		public void shutdown() {
			proxied.shutdown();
		}

		@Override
		public boolean isShutdown() {
			return proxied.isShutdown();
		}

		@Override
		public <T> Future<T> download(CombinedDownloadTask<T> task, CombinedDownloadCallback<T> callback) {
			task = task.cacheable(CacheStrategy.FORCIBLY_CACHE);
			return proxied.download(task, callback);
		}

		@Override
		public <T> Future<T> download(DownloadTask<T> task, DownloadCallback<T> callback) {
			task = task.cacheable();
			return proxied.download(task, callback);
		}

		@Override
		public <T> Future<T> download(CombinedDownloadTask<T> task, CombinedDownloadCallback<T> callback, int tries) {
			task = task.cacheable(CacheStrategy.FORCIBLY_CACHE);
			return proxied.download(task, callback, tries);
		}

		@Override
		public <T> Future<T> download(DownloadTask<T> task, DownloadCallback<T> callback, int tries) {
			task = task.cacheable();
			return proxied.download(task, callback, tries);
		}

	}

	private Builder<CombinedDownloader> wrapCombinedDownloader(Builder<CombinedDownloader> o) {
		return () -> new ProxiedCombinedDownloader(o.build());
	}

	private Builder<Downloader> wrapDownloader(Builder<Downloader> o) {
		return () -> new ProxiedDownloader(o.build());
	}

	private Launcher launcher;
	private MinecraftDownloader downloader;

	public LaunchHandler() {
		launcher = LauncherBuilder.create()
				.printDebugCommandline(true)
				.build();
		downloader = MinecraftDownloaderBuilder.create(wrapCombinedDownloader(
				CombinedDownloaderBuilder.create(
						CachedDownloaderBuilder.create(wrapDownloader(DownloaderBuilders.downloader()))
								.ehcache(CacheManagerBuilder.newCacheManagerBuilder()

										.withCache("org.to2mbn.jmccc.mcdownloader.cache.dynamic", CacheConfigurationBuilder
												.newCacheConfigurationBuilder(URI.class, byte[].class, ResourcePoolsBuilder.newResourcePoolsBuilder()
														.disk(1, MemoryUnit.GB, true))
												.withExpiry(Expirations.timeToLiveExpiration(new Duration(6, TimeUnit.HOURS))))

										.withCache("org.to2mbn.jmccc.mcdownloader.cache.static", CacheConfigurationBuilder
												.newCacheConfigurationBuilder(URI.class, byte[].class, ResourcePoolsBuilder.newResourcePoolsBuilder()
														.disk(10, MemoryUnit.GB, true))
												.withExpiry(Expirations.noExpiration()))

										.with(CacheManagerBuilder.persistence("caches"))))))
				.providerChain(DownloadProviderChain.create()
						.baseProvider(new MojangDownloadProvider() {

							@Override
							protected String getAssetBaseURL() {
								return "https://authentication.x-speed.cc/minecraft/resources/";
							}

						})
						.addProvider(new ForgeDownloadProvider())
						.addProvider(new LiteloaderDownloadProvider()))
				.build();

	}

	public void testJmccc(File mcdirPath, String version) throws TestFailureException {
		try {
			MinecraftDirectory mcdir = new MinecraftDirectory(mcdirPath);
			Version v = downloader.downloadIncrementally(mcdir, version, null).get();

			LaunchOption option = new LaunchOption(v, new OfflineAuthenticator("testuser"), mcdir);
			option.extraJvmArguments().add(ExtraArgumentsTemplates.FML_IGNORE_INVALID_MINECRAFT_CERTIFICATES);
			option.extraJvmArguments().add(ExtraArgumentsTemplates.FML_IGNORE_PATCH_DISCREPANCISE);

			AtomicLong lastOutput = new AtomicLong(System.currentTimeMillis());
			Process process = launcher.launch(option, new ProcessListener() {

				@Override
				public void onLog(String log) {
					System.err.println("[stdout] " + log);
					lastOutput.set(System.currentTimeMillis());
				}

				@Override
				public void onErrorLog(String log) {
					System.err.println("[stderr] " + log);
					lastOutput.set(System.currentTimeMillis());
				}

				@Override
				public void onExit(int code) {
					System.err.println("[exit] exit code: " + code);
				}
			});

			boolean succeed = false;
			try {
				while (process.isAlive()) {
					Thread.sleep(500);
					if (lastOutput.get() + LAUNCH_WAIT_TIME <= System.currentTimeMillis()) {
						succeed = true;
						System.err.println("Test passed, stopping subprocess");
						break;
					}
				}
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new RuntimeException("Thread interrupted", e);
			} finally {
				process.destroyForcibly();
			}

			while (process.isAlive())
				Thread.sleep(500);

			if (!succeed)
				throw new LaunchException("Exited with code: " + process.exitValue());

		} catch (Throwable e) {
			throw new TestFailureException("Test Failure: " + version, e);
		}
	}

	public MinecraftDownloader getDownloader() {
		return downloader;
	}

	public void shutdown() {
		downloader.shutdown();
	}

}
